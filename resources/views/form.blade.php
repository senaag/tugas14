<!DOCTYPE html>
<html>

<head>
    <title>Tugas 12 - sena</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lastname"><br><br>

        <label>Gender:</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>

        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="english">English</option>
            <option value="china">China</option>
            <option value="other">other</option>

        </select> <br> <br>

        <label>Languange Spoken:</label><br>
        <input type="checkbox" name="bahasa indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other <br><br>

        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br><br>

        <input type="submit" value="Submit">
    </form>
</body>

</html>